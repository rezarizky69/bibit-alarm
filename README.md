# flutter_bibit

Project alarm for the task from Bibit. This application can display an alarm based on the hour and minute hand that is driven by the user.

Usage flow:
- Move the hour and minute hand as desired.
- Set time am or pm.
- Click the toggle button to activate notification.
- When a notification appears, tap the notification, a bar chart will appear containing the time difference when the notification is launched and when the user taps the notification.

Link to project demo : https://drive.google.com/file/d/11ryH5jTD3PzWbbiQdTL-C4XJLEALeLzD/view?usp=sharing

*This may not be perfect, because perfection only belongs to God*
