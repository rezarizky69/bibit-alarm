import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/subjects.dart';
import 'package:timezone/timezone.dart' as tz;

// class that provide service for notification via local notification
class NotificationService {
  // notification declare
  static final _notifications = FlutterLocalNotificationsPlugin();
  // the behavior that act on stream
  static final onNotification = BehaviorSubject<String?>();

  // initialization notification
  static Future init({bool initScheduled = false}) async {
    const android = AndroidInitializationSettings('@mipmap/ic_launcher');
    const ios = IOSInitializationSettings();
    const initializationSettings =
        InitializationSettings(android: android, iOS: ios);
    await _notifications.initialize(
      initializationSettings,
      onSelectNotification: (payload) async {
        onNotification.add(payload);
      },
    );
  }

  // function for showing scheduled notification 
  static Future showNotificationSchedule({
    int id = 0,
    String? title,
    String? body,
    String? payload,
    required DateTime scheduleDate,
  }) async =>
      _notifications.zonedSchedule(
        id,
        title,
        body,
        // represent the time schedule for notification
        tz.TZDateTime.from(scheduleDate, tz.local),
        await notificationDetails(),
        payload: payload,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
      );

  // the detail property of notification channel
  static Future notificationDetails() async {
    const sound = 'soundalarm.wav';
    return const NotificationDetails(
      android: AndroidNotificationDetails(
        'bibit',
        'bibitAlarm',
        channelDescription: 'This is alarm notification by Bibit',
        importance: Importance.max,
        priority: Priority.high,
        playSound: true,
        sound: RawResourceAndroidNotificationSound('soundalarm'),
        enableVibration: true,
        fullScreenIntent: true,
      ),
      iOS: IOSNotificationDetails(
        sound: sound,
      ),
    );
  }

  // cancelling notification
  static void cancel() => _notifications.cancelAll();
}
