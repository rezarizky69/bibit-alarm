import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'minute_event.dart';
part 'minute_state.dart';

// class that process the logic and send to the ui
class MinuteBloc extends Bloc<MinuteEvent, MinuteState> {
  MinuteBloc() : super(MinuteInitial()) {

    // when the event is set minute
    on<SetMinute>((event, emit) {

      // should convert the minute into string
      var minute = event.minute.toString();

      // if the length of minute less than 2 digits, add '0' string
      if (minute.length < 2) {
        minute = '0' + minute;
      }

      emit(LoadedMinute(minute: minute));
    });
  }
}
