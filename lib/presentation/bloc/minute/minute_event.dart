part of 'minute_bloc.dart';

// class that represent an event to the bloc
@immutable
abstract class MinuteEvent {}

// event that take an int of minute
class SetMinute extends MinuteEvent {
  final int minute;

  SetMinute(this.minute);

  List<Object> get props => [minute];
}
