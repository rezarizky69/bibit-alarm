part of 'minute_bloc.dart';

// class that represent the state emitted by bloc later
@immutable
abstract class MinuteState {}

// first state, is initial
class MinuteInitial extends MinuteState {}

// last state, is loaded state that output a string of minute
class LoadedMinute extends MinuteState {
  final String minute;

  LoadedMinute({required this.minute});
}