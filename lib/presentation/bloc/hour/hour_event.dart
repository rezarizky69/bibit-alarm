part of 'hour_bloc.dart';

// class that represent an event to the bloc
@immutable
abstract class HourEvent {}

// event that take an int of hour 
class SetHour extends HourEvent {
  final int hour;

  SetHour(this.hour);

  List<Object> get props => [hour];
}