import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'hour_event.dart';
part 'hour_state.dart';

// class that process the logic and send to the ui
class HourBloc extends Bloc<HourEvent, HourState> {
  HourBloc() : super(HourInitial()) {

    // when the event is set hour
    on<SetHour>((event, emit) {

      // should convert the hour into string
      var hour = event.hour.toString();

      // if the length of hour less than 2 digits, add '0' string
      if (hour.length < 2) {
        hour = '0' + hour;
      }

      emit(LoadedHour(hour: hour));
    });
  }
}
