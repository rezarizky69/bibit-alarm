part of 'hour_bloc.dart';

// class that represent the state emitted by bloc later
@immutable
abstract class HourState {}

// first state, is initial
class HourInitial extends HourState {}

// last state, is loaded state that output a string of hour
class LoadedHour extends HourState {
  final String hour;

  LoadedHour({required this.hour});
}
