import 'dart:async';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter_bibit/presentation/bloc/hour/hour_bloc.dart';
import 'package:flutter_bibit/presentation/bloc/minute/minute_bloc.dart';
import 'package:flutter_bibit/presentation/clock/clock.dart';
import 'package:flutter_bibit/presentation/widgets/bar_chart.dart';
import 'package:flutter_bibit/util/notification_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String hour = '0';
  String minute = '0';
  bool isActived = false;
  bool isAm = true;
  List<bool> isSelected = List.generate(2, (index) => false);

  @override
  void initState() {
    super.initState();
    NotificationService.init();
    listenNotification();
    isSelected[0] = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: buldAppbar(),
      body: SingleChildScrollView(
        child: buildBody(),
      ),
    );
  }

  // function for build body 
  buildBody() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 40),
          child: buildHourMinute(),
        ),
        buildAmPm(),
        const Clock(),
        const SizedBox(height: 20),
        buildEvent(),
      ],
    );
  }

  // function for build appbar
  buldAppbar() {
    return AppBar(
      title: const Text(
        'Notify Me',
        style: TextStyle(
          color: Color(0xffff9800),
          fontWeight: FontWeight.bold,
        ),
      ),
      backgroundColor: Colors.black,
      elevation: 0,
      centerTitle: false,
    );
  }

  // function for displaying the hour and minute of the time
  buildHourMinute() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        BlocBuilder<HourBloc, HourState>(
          builder: (context, state) {
            Future.delayed(Duration.zero, () {
              if (state is LoadedHour) {
                setState(() {
                  hour = state.hour;
                });
              }
            });
            return Text(
              state is LoadedHour ? state.hour : '00',
              style: const TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            );
          },
        ),
        const SizedBox(
          width: 5,
        ),
        const Text(
          ':',
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        const SizedBox(
          width: 5,
        ),
        BlocBuilder<MinuteBloc, MinuteState>(
          builder: (context, state) {
            Future.delayed(Duration.zero, () {
              if (state is LoadedMinute) {
                setState(() {
                  minute = state.minute;
                });
              }
            });
            return Text(
              state is LoadedMinute ? state.minute : '00',
              style: const TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            );
          },
        ),
      ],
    );
  }

  // function for switching between am or pm for the time selected
  buildAmPm() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, top: 20, right: 20),
      child: ToggleButtons(
        selectedColor: const Color(0xffff9800),
        color: Colors.white,
        children: const [
          Text(
            'AM',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'PM',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          )
        ],
        onPressed: (int index) {
          setState(() {
            for (int buttonIndex = 0;
                buttonIndex < isSelected.length;
                buttonIndex++) {
              if (buttonIndex == index) {
                isAm = index == 0 ? true : false;
                isActived = false;
                isSelected[buttonIndex] = true;
                NotificationService.cancel();
              } else {
                isSelected[buttonIndex] = false;
              }
            }
          });
        },
        isSelected: isSelected,
      ),
    );
  }

  // function that trigger on or off the notification from notification service
  buildEvent() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Align(
          alignment: Alignment.center,
          child: Text(
            isActived ? 'Active' : 'Off',
            style: TextStyle(
              color: isActived ? const Color(0xffff9800) : Colors.red,
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Switch(
            inactiveTrackColor: Colors.white,
            value: isActived,
            activeColor: const Color(0xffff9800),
            onChanged: (value) {
              setState(() {
                isActived = value;
                if (isActived) {
                  NotificationService.showNotificationSchedule(
                      title: 'Bibit',
                      body: 'Yuk investasi di Bibit!',
                      payload: setDateTime().toString(),
                      scheduleDate: setDateTime());
                  Future.delayed(
                      Duration(
                        seconds: setDateTime()
                            .difference(
                              DateTime.now(),
                            )
                            .inSeconds,
                      ), () {
                    isActived = false;
                  });
                  showSnackBar();
                } else {
                  NotificationService.cancel();
                }
              });
            }),
      ],
    );
  }

  // function to listen the stream from notification
  void listenNotification() =>
      NotificationService.onNotification.stream.listen(onClickNotification);

  // handle the callback when notification tapped
  void onClickNotification(String? payload) {
    // showing modal with bar chart displaying the time difference between
    // notification launched and user tapped
    showModalBottomSheet<void>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      context: context,
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: BarChart(
            data: [
              TimeOpen(
                payload,
                DateTime.now().difference(DateTime.parse(payload!)).inSeconds,
                charts.ColorUtil.fromDartColor(
                  const Color(0xffff9800),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  // showing snackbar 
  void showSnackBar() {
    // format of the date displayed
    String formattedDate = DateFormat('yyyy-MM-dd  hh:mm a').format(
      setDateTime(),
    );
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          'Alarm active for $formattedDate',
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
        dismissDirection: DismissDirection.startToEnd,
      ),
    );
  }

  // handle the datetime when selected
  DateTime setDateTime() {
    DateTime now = DateTime.now();
    return DateTime(
        now.year,
        now.month,
        now.hour > (isAm ? int.parse(hour) : int.parse(hour) + 12)
            ? now.day + 1
            : now.hour == (isAm ? int.parse(hour) : int.parse(hour) + 12) &&
                    now.minute >= int.parse(minute) &&
                    now.second > 0
                ? now.day + 1
                : now.day,
        isAm ? int.parse(hour) : int.parse(hour) + 12,
        int.parse(minute));
  }
}
