import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart' as vector;

// class that represent the circle of the clock
class CirclePainter extends CustomPainter {
  double circleSize;
  double longNeedleHeight;
  double shortNeedleHeight;
  double radius = 0;
  BuildContext context;

  CirclePainter(
      {required this.circleSize,
      required this.longNeedleHeight,
      required this.shortNeedleHeight,
      required this.context}) {
    radius = circleSize / 2;
  }

  // function for creating the circle using custom painter
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();

    for (int i = 0; i < 60; i++) {
      double minute = 360 / 60 * i;

      // the condition (i % 5 == 0) are used for differentiate between
      // longNeedleHeight and shortNeedleHeight
      paint.color = (i % 5 == 0) ? const Color(0xffff9800) : Colors.white;
      paint.strokeWidth = (i % 5 == 0) ? 4 : 1;

      double distance = (i % 5 == 0) ? longNeedleHeight : shortNeedleHeight;

      double x1 = radius * cos(vector.radians(minute));
      double y1 = radius * sin(vector.radians(minute));
      final p1 = Offset(x1, y1);

      double x2 = (radius - distance) * cos(vector.radians(minute));
      double y2 = (radius - distance) * sin(vector.radians(minute));
      final p2 = Offset(x2, y2);

      canvas.drawLine(p1, p2, paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
