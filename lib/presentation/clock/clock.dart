import 'package:flutter/material.dart';

import 'hour_hand.dart';
import 'minute_hand.dart';
import 'circle_painter.dart';

// class that represent the clock based on all widgets
class Clock extends StatefulWidget {
  const Clock({Key? key}) : super(key: key);

  @override
  _ClockState createState() => _ClockState();
}

class _ClockState extends State<Clock> with TickerProviderStateMixin {
  double circleSize = 300;
  final double longNeedleHeight = 40;
  final double shortNeedleHeight = 25;

  @override
  Widget build(BuildContext context) {
    CirclePainter circlePainter = CirclePainter(
      circleSize: circleSize,
      longNeedleHeight: longNeedleHeight,
      shortNeedleHeight: shortNeedleHeight,
      context: context,
    );

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
        ),
        Stack(
          alignment: Alignment.center,
          children: [
            // first layer is the outer circle that have short and long needle
            // represent the second 
            SizedBox(
              width: circleSize,
              height: circleSize,
              child: Container(
                color: Colors.transparent,
                child: Center(
                  child: CustomPaint(
                    painter: circlePainter,
                  ),
                ),
              ),
            ),
            // next layer is the circle and have the dots at the center
            Container(
              width: circleSize,
              height: circleSize,
              color: Colors.transparent,
              child: Center(
                child: Container(
                  width: 15,
                  height: 15,
                  decoration: BoxDecoration(
                    color: const Color(0xffff9800),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ),
            // the needle of minute
            const MinuteHand(),
            // the needle of hour
            const HourHand(),
          ],
        )
      ],
    );
  }
}
