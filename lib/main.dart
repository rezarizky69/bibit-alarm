import 'package:flutter/material.dart';
import 'package:flutter_bibit/presentation/bloc/hour/hour_bloc.dart';
import 'package:flutter_bibit/presentation/bloc/minute/minute_bloc.dart';
import 'package:flutter_bibit/presentation/main/home.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:timezone/data/latest.dart' as tz;

void main() {
  // initialization of time zone
  tz.initializeTimeZones();
  // wrapping the entire app with multi bloc provider
  runApp(MultiBlocProvider(providers: [
    BlocProvider<HourBloc>(
      create: (context) => HourBloc(),
    ),
    BlocProvider<MinuteBloc>(
      create: (context) => MinuteBloc(),
    ),
  ], child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bibit Alarm',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const Home(),
    );
  }
}
